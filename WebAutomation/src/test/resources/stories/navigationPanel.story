Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Given I open panels using Panels_URL
When I type <User_Name> in field UserName
And I type <Password> in field Password
And I click on button SignIn


Examples:
|User_Name              |Password       |
|camsteam@snapdeal.com  |Snap@4756$!    |